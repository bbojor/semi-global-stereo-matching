#include <iostream>
#include <opencv2/opencv.hpp>
#include <fstream>
#include <string>
#include <utility>
#include <unistd.h>
#include <limits>
#include <sstream>
#include <chrono>

using namespace std;
using namespace cv;

// the maximum disparity(offset) at which we search for pixels
#define DISPARITY 64

// the number of paths through the image we use to aggregate the cost for each pixel
#define NO_PATHS 8

// small discontinuity penalty
#define P1 15

// large discontinuity penalty
#define P2 40 

// value used to initialize the various cost matrices or to use
// in the calculation of path costs
#define OUT_OF_BOUNDS_VALUE std::numeric_limits<int>::max()

// value used when evaluating our result
#define ERROR_MARGIN 3

//the 8 paths used to compute the cost of each pixel
typedef struct direction
{
    int dx,dy;
} 
Direction;

enum direction_name {
    LEFT,
    TOP_LEFT,
    TOP,
    TOP_RIGHT,

    RIGHT,
    BOTTOM_LEFT,
    BOTTOM,
    BOTTOM_RIGHT
    };

Direction directions[] = {
    { 0,-1},
    {-1,-1},
    {-1, 0},
    {-1, 1},

    { 0, 1},
    { 1,-1},
    { 1, 0},
    { 1, 1}, 
};

//browse for input images using a python script
vector<string> read_images()
{
    //run python script
    system("python file_browser.py");
    system("clear");

    fstream file;
    file.open("files", ios::in);
    vector<string> files;

    if(file.is_open())
    {
        string line;

        getline(file, line);
        int no_files = stoi(line);


        for(int i = 0; i < no_files; i++)
        {
            getline(file, line);
            files.push_back(line);  
        }

         //cleanup file
        unlink("files");
    }
        
    return files;
}

//compute a simple cost for pairs of pixels by taking the absolute value of 
//the difference between their intensities
//some pixels to the left of the image are left out of this computation since they 
//do not appear in the right image based on the assumed disparity
void compute_simple_cost(Mat left, Mat right, int*** pixel_costs)
{
    for(int i = 0; i < left.rows; i ++) 
    {
        for(int j = DISPARITY; j < left.cols; j ++)
        {
            for(int k = 0; k < DISPARITY; k++)
            {
                pixel_costs[i][j][k] = abs(left.at<uchar>(i, j) - right.at<uchar>(i, j - k));
            }
        }
    }
}

//finds the minimum of the given path in the cost array
int find_minimum_in_path(int* path)
{
    int min = std::numeric_limits<int>::max();
    for(int i = 0; i < DISPARITY; i ++)
    {
        if(path[i] < min)
            min = path[i];
    }

    return min;
}

//checks wheter the point with coordinates (i,j,k) is contained in the paralellipiped with 
//dimensions (i_max, j_max, k_max) 
bool is_in_array(int i,int j, int k, int i_max, int j_max, int k_max)
{
    return (i >= 0) && (j >= 0) && (k >= 0) && (i < i_max) && (j < j_max) && (k < k_max);
}

// computes a path for each pixel, for each of the 8 defined paths
void compute_path_costs(int*** pixel_costs, int**** path_costs, int rows, int cols)
{   
    for(int path = 0; path < NO_PATHS; path++)
    {   
        Direction direction = directions[path];
        if(path == LEFT || path == TOP_LEFT || path == TOP || path == TOP_RIGHT)
        {
            // process from the top-left corner of the image 
            for(int i = 0; i < rows; i ++) 
            {
                for(int j = 0; j < cols; j ++)
                {
                    for(int k = 0; k < DISPARITY; k++)
                    {
                        int simple_cost = pixel_costs[i][j][k]; // the C value from the paper

                        //now we need to determine the min value from the formula
                        int previous_pixel = is_in_array(i + direction.dx, j + direction.dy, k, rows, cols, DISPARITY) ?
                            path_costs[path][i + direction.dx][j + direction.dy][k] : // Lr(p-r,d)
                            OUT_OF_BOUNDS_VALUE;
                            
                        int previous_pixel_smaller_disparity = is_in_array(i + direction.dx, j + direction.dy, k - 1, rows, cols, DISPARITY) ?
                            path_costs[path][i + direction.dx][j + direction.dy][k - 1] + P1 : // Lr(p-r,d-1) + P1
                            OUT_OF_BOUNDS_VALUE; 

                        int previous_pixel_larger_disparity = is_in_array(i + direction.dx, j + direction.dy, k + 1, rows, cols, DISPARITY) ?
                            path_costs[path][i + direction.dx][j + direction.dy][k + 1] + P1 : // Lr(p-r, d+1) + P1
                            OUT_OF_BOUNDS_VALUE;

                        // we hold this value in a separate variable because we will also need to substract it at the end
                        int minimum_previous_cost = is_in_array(i + direction.dx, j + direction.dy, 0, rows, cols, DISPARITY) ?
                            find_minimum_in_path(path_costs[path][i+ direction.dx][j + direction.dy]) : // min Lr(p-r,i) == min Lr(p-r, k)
                            OUT_OF_BOUNDS_VALUE;

                        int previous_pixel_any_disparity = minimum_previous_cost + P2; // min Lr(p-r,i) + P2

                        //Lr(p,d)
                        int final_value = simple_cost +  
                            min(previous_pixel, min(previous_pixel_smaller_disparity, min(previous_pixel_larger_disparity, previous_pixel_larger_disparity))) 
                            - minimum_previous_cost;
                        
                        path_costs[path][i][j][k] = final_value;
                    }
                }
            }
        }
        else
        {
            //from the bottom-right
            for(int i = rows - 1; i >= 0; i--) 
            {
                for(int j = cols - 1; j >= 0; j--)
                {
                    for(int k = 0; k < DISPARITY; k++)
                    {
                        int simple_cost = pixel_costs[i][j][k]; // the C value from the paper

                        //now we need to determine the min value from the formula
                        int previous_pixel = is_in_array(i + direction.dx, j + direction.dy, k, rows, cols, DISPARITY) ?
                            path_costs[path][i + direction.dx][j + direction.dy][k] : // Lr(p-r,d)
                            OUT_OF_BOUNDS_VALUE;
                            
                        int previous_pixel_smaller_disparity = is_in_array(i + direction.dx, j + direction.dy, k - 1, rows, cols, DISPARITY) ?
                            path_costs[path][i + direction.dx][j + direction.dy][k - 1] + P1 : // Lr(p-r,d-1) + P1
                            OUT_OF_BOUNDS_VALUE; 

                        int previous_pixel_larger_disparity = is_in_array(i + direction.dx, j + direction.dy, k + 1, rows, cols, DISPARITY) ?
                            path_costs[path][i + direction.dx][j + direction.dy][k + 1] + P1 : // Lr(p-r, d+1) + P1
                            OUT_OF_BOUNDS_VALUE;

                        // we hold this value in a separate variable because we will also need to substract it at the end
                        int minimum_previous_cost = is_in_array(i + direction.dx, j + direction.dy, 0, rows, cols, DISPARITY) ?
                            find_minimum_in_path(path_costs[path][i+ direction.dx][j + direction.dy]) : // min Lr(p-r,i) == min Lr(p-r, k)
                            OUT_OF_BOUNDS_VALUE;
                             
                        int previous_pixel_any_disparity = minimum_previous_cost + P2; // min Lr(p-r,i) + P2

                        //Lr(p,d)
                        int final_value = simple_cost +  
                            min(previous_pixel, min(previous_pixel_smaller_disparity, min(previous_pixel_larger_disparity, previous_pixel_larger_disparity))) 
                            - minimum_previous_cost;
                        
                        path_costs[path][i][j][k] = final_value;
                    }
                }
            }
        }
        cout << endl << "Finished computing costs for path "  << path << endl;
    }
}

// for each pixel, sum the 8 path costs to obtain a final aggregated cost
void compute_aggregate_costs(int**** path_costs, int*** aggregate_costs, int rows, int cols)
{
    for(int i = 0; i < rows; i ++) 
    {
        for(int j = 0; j < cols; j ++)
        {
            for(int k = 0; k < DISPARITY; k++)
            {
                for(int path = 0; path < NO_PATHS; path++)
                {   
                    aggregate_costs[i][j][k] += path_costs[path][i][j][k];
                }
            }
        }
    }
}

// create a disparity image representing the depth of various objects in the image
// the values in the disparity map represent the disparity(offset) for which 
// the matching cost of each pixel is minimum (i.e. how much it was shifted in one image compared to the other)
Mat get_disparity_from_aggregated_costs(int*** aggregated_costs, int rows, int cols)
{   
    Mat disparity(rows, cols, CV_8UC1);

    // disparity values are in the [0,DISPARITY] interval, 
    // we can multiply them by a constant in order to obtain more reedable images with intensity values  in [0,255]
    float multiplier = 255.0f / DISPARITY;

    for(int i = 0; i < rows; i ++)
    {
        for(int j = 0; j < cols; j++)
        {   
            int min = aggregated_costs[i][j][0];
            int min_disp = 0;
            //for each pixel find the disparity value (offset) which provides the smallest cost
            for(int k = 1; k < DISPARITY; k++)
            {
                if(aggregated_costs[i][j][k] < min)
                {
                    min_disp = k;
                    min = aggregated_costs[i][j][k];
                }             
            }
          
            disparity.at<uchar>(i,j) = multiplier * min_disp;
        }
    }
    return disparity;
}

// Scores the obtained image based on how many pixel are outside 
// the error margin. Returns the percentage of offending pixels.
// Occlusions are ignored  
int evaluate(Mat result, Mat ground_truth)
{   
    int error = 0; 

    for(int i = 0; i < result.rows; i++)
    {
        for(int j = 0; j < result.cols; j++)
        {
            if(ground_truth.at<uchar>(i,j) != 0)
            {
                if(abs(ground_truth.at<uchar>(i,j) - result.at<uchar>(i,j)) > ERROR_MARGIN)
                    error++;
            }
        }
    }

    return error * 100 / (result.rows * result.cols);
}

int main(int argc, char* argv[])  
{	
    // read the input images
    vector<string> images = read_images();
    if(images.size() != 2)
    {
        cout << "Need to provide two images, the left and right image respectively!\n";
        exit(1);
    }

    cout<< "Left image: " << images.at(0).c_str() << endl;
    cout<< "Right image: " << images.at(1).c_str() << endl;

    Mat left = imread(images.at(0).c_str(), IMREAD_GRAYSCALE);
    Mat right = imread(images.at(1).c_str(), IMREAD_GRAYSCALE);

    // initialize the needed arrays 
    //this will hold the matching cost between pixels from the two images at various disparities(offsets)
    // afther the path costs are computed it will hold the
    // aggregated costs for each pixel (basically the sum of the costs from  each path)
    int*** pixel_costs;

    //this will hold the cost for each pixel, for each of the 8 paths
    int**** path_costs;

    //
    pixel_costs = (int***)malloc(left.rows * sizeof(int**));

    for(int i = 0; i <left.rows; i++)
    {
        pixel_costs[i] = (int**)malloc(left.cols * sizeof(int*));

        for(int j = 0; j <left.cols; j++)
        {
            pixel_costs[i][j] = (int*)malloc(DISPARITY * sizeof(int));
            memset(pixel_costs[i][j], OUT_OF_BOUNDS_VALUE, DISPARITY * sizeof(int));
        }
    }

    path_costs = (int****)malloc(NO_PATHS * sizeof(int***));
    for(int k = 0; k < NO_PATHS; k ++)
    {
        path_costs[k] = (int***)malloc(left.rows * sizeof(int**));
        for(int i = 0; i <left.rows; i++)
        {
            path_costs[k][i] = (int**)malloc(left.cols * sizeof(int*));

            for(int j = 0; j <left.cols; j++)
            {
                path_costs[k][i][j] = (int*)malloc(DISPARITY * sizeof(int));
                memset(path_costs[k][i][j],  OUT_OF_BOUNDS_VALUE, DISPARITY * sizeof(int));
            }
        }
    }

    //display the input images 
    imshow("left", left);
    imshow("right", right);
    waitKey(0);
    
    // >>>> ACTUAL ALGORITHM STARTS HERE <<<<

    //measure time
    auto t1 = chrono::high_resolution_clock::now();
    
    compute_simple_cost(left, right, pixel_costs);
    cout << "Computed costs\n";

    compute_path_costs(pixel_costs, path_costs, left.rows, left.cols);
    cout << "\nComputed paths\n";

    compute_aggregate_costs(path_costs, pixel_costs, left.rows, left.cols);
    cout << "\nComputed aggregate\n";

    // create and display the final disparity image
    Mat disparity_image = get_disparity_from_aggregated_costs(pixel_costs, left.rows, left.cols);

    auto t2 = chrono::high_resolution_clock::now();

    auto miliseconds = chrono::duration_cast<chrono::milliseconds>(t2 - t1);
    cout << "Time: " << miliseconds.count() / 1000.0f << " seconds" << endl;

    imshow("disparity_image", disparity_image);
    waitKey(0);

    stringstream file_name;

    file_name << "../../results/teddy.png";

    imwrite(file_name.str(), disparity_image);

    // compare result with ground truth
    Mat ground_truth = imread("../../ground_truth/teddy.png", IMREAD_GRAYSCALE);
    int error = evaluate(disparity_image, ground_truth);
    cout << "Error: " << error << endl;
}    